package hr.fer.ruazosa.lecture5demo

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var doInBackGround: AsyncTask<Int, Int, Unit>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startLongOperationButton.setOnClickListener {
            doInBackGround = DoInBackGround()
            doInBackGround?.execute(6)
        }
        sayHelloButton.setOnClickListener {
            val toast = Toast.makeText(applicationContext, "Say Hello", Toast.LENGTH_LONG)
            toast.show()
        }
    }

    override fun onDestroy() {
        doInBackGround?.cancel(true)
        super.onDestroy()
    }

    inner class DoInBackGround: AsyncTask<Int, Int, Unit>() {
        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            operationStatusTextView.text = "Operation not started"
            progressBar.visibility = View.INVISIBLE
        }
        override fun doInBackground(vararg params: Int?) {
            val operationLenghtInSeconds = params[0]
            for (i in 0..operationLenghtInSeconds!!) {
                if (this.isCancelled) {
                    break
                }
                Thread.sleep(1000)
                this.publishProgress(i)
            }
        }
        override fun onProgressUpdate(vararg values: Int?) {
            showProgressTextView.text = "" + values[0]!!
        }
        override fun onPreExecute() {
            super.onPreExecute()
            operationStatusTextView.text = "Long running operation started"
            progressBar.visibility = View.VISIBLE
        }
    }
}
